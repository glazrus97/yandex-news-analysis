<?php namespace Egorov\NewsAnalysis\Models;

use Model;

/**
 * Model
 */
class NewsExport extends \Backend\Models\ExportModel
{
    public $belongsTo = [
        'media' => 'Egorov\NewsAnalysis\Models\Media',
        'rubric' => 'Egorov\NewsAnalysis\Models\Rubrics',
    ];

    public $hasMany = [
        'newsData' => 'Egorov\NewsAnalysis\Models\NewsData',
    ];

    public function exportData($columns, $sessionKey = null)
    {
        $records = News::with('media', 'rubric')->get();
        $records->each(function($news) use ($columns) {
            $news = array_add($news, 'id', "");
            $news = array_add($news, 'title', "");
            $news = array_add($news, 'url', "");
            $news = array_add($news, 'content', "");
            $news = array_add($news, 'rubric_id', "");
            $news = array_add($news, 'media_id', "");
            $news = array_add($news, 'created_at', "");

            $news->addVisible($columns);
        });
        return $records->toArray();
    }
}
