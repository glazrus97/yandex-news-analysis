<?php namespace Egorov\NewsAnalysis\Models;

use Model;

/**
 * Model
 */
class RecordsExport extends \Backend\Models\ExportModel
{
    public $belongsToMany = [
        'news' => ['Egorov\NewsAnalysis\Models\News', 'table' => 'egorov_newsanalysis_records_news']
    ];

    public $belongsTo = [
        'request' => 'Egorov\NewsAnalysis\Models\Requests',
    ];

    public function exportData($columns, $sessionKey = null)
    {
        $records = Records::with('news', 'request')->get();
        $records->each(function($news) use ($columns) {
            $news = array_add($news, 'id', "");
            $news = array_add($news, 'request_id', "");
            $news = array_add($news, 'comments', "");

            $news->addVisible($columns);
        });
        return $records->toArray();
    }
}
