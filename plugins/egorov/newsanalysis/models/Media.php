<?php namespace Egorov\NewsAnalysis\Models;

use Model;

/**
 * Model
 */
class Media extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    protected $fillable = [
        'name'
    ];
    /**
     * @var string The database table used by the model.
     */
    public $table = 'egorov_newsanalysis_media';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $hasMany = [
        'news' => 'Egorov\NewsAnalysis\Models\News',
    ];
}
