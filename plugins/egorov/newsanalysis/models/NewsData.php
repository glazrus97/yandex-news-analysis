<?php namespace Egorov\NewsAnalysis\Models;

use Model;

/**
 * Model
 */
class NewsData extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'egorov_newsanalysis_news_data';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'news' => 'Egorov\NewsAnalysis\Models\News',
    ];
}
