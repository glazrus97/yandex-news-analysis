<?php namespace Egorov\NewsAnalysis\Models;

use Model;

/**
 * Model
 */
class Requests extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['owner', 'email', 'phone', 'description', 'status'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'egorov_newsanalysis_requests';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $hasMany = [
        'records' => 'Egorov\NewsAnalysis\Models\Records',
    ];
}
