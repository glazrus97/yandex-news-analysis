<?php namespace Egorov\NewsAnalysis\Models;

use Backend\Models\ImportModel;
use Exception;
use Egorov\NewsAnalysis\Models\News;

/**
 * Model
 */
class NewsImport extends ImportModel
{
    public $rules = [
    ];

    public function importData($results, $sessionKey = null) {
        foreach ($results as $row => $data) {

            try {
                $news = new News();
                $news->title = $data['title'];
                $news->url = $data['url'];
                $news->content = $data['content'];
                $news->rubric_id = $data['rubric_id'];
                $news->media_id = $data['media_id'];
                $news->save();

                $this->logCreated();
            }
            catch (Exception $e) {
                $this->logError($row, $e->getMessage());
            }

        }

    }
}
