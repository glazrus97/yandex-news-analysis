<?php namespace Egorov\NewsAnalysis\Models;

use Model;

/**
 * Model
 */
class News extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'title',
        'url',
        'content',
        'rubric_id',
        'media_id'
    ];
    /**
     * @var string The database table used by the model.
     */
    public $table = 'egorov_newsanalysis_news';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'media' => 'Egorov\NewsAnalysis\Models\Media',
        'rubric' => 'Egorov\NewsAnalysis\Models\Rubrics',
    ];

    public $hasMany = [
        'newsData' => 'Egorov\NewsAnalysis\Models\NewsData',
    ];
}
