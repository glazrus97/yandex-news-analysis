<?php namespace Egorov\NewsAnalysis\Models;

use Model;

/**
 * Model
 */
class Rubrics extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'egorov_newsanalysis_rubrics';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $hasMany = [
        'news' => 'Egorov\NewsAnalysis\Models\News',
    ];
}
