<?php namespace Egorov\NewsAnalysis\Models;

use Model;

/**
 * Model
 */
class Records extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'egorov_newsanalysis_record';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsToMany = [
        'news' => ['Egorov\NewsAnalysis\Models\News', 'table' => 'egorov_newsanalysis_records_news']
    ];

    public $belongsTo = [
        'request' => 'Egorov\NewsAnalysis\Models\Requests',
    ];
}
