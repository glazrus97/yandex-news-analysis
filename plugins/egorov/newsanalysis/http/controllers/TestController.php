<?php

namespace Egorov\NewsAnalysis\Http\Controllers;

use Illuminate\Routing\Controller;
use Egorov\NewsAnalysis\Classes\CrawlerHelper;
use Jenssegers\Date\Date;
use Symfony\Component\DomCrawler\Crawler;

class TestController extends Controller
{

    public function index()
    {
        $link = 'https://yandex.ru/news/rubric/society';
        $html = file_get_contents($link);
        $crawler = new Crawler(null, $link);
        $crawler->addHtmlContent($html, 'UTF-8');

        $stories = $crawler->filter(".story")->each(function (Crawler $node, $i) {
            $story['title'] = $node->filter(".story__title")->text();
            $story['url'] = 'https://yandex.ru' . $node->filter(".story__title > .link")->attr('href');
            $storyCrawler = new Crawler(null, $story['url']);
            $storyCrawler->addHtmlContent(file_get_contents($story['url']), 'UTF-8');

            $story['content'] = $storyCrawler->filter(".doc__text")->text();
            $story['rubric_id'] = 1;
            $story['media_id'] = $storyCrawler->filter('.story__head-image')->attr('alt');//firstOrCreate
            $story['publication_date'] = new Date();
            return $story;
        });
        dd($stories);
    }

    /*
        $link = 'https://yandex.ru/news/';
        $html = file_get_contents($link);
        $crawler = new Crawler(null, $link);
        $crawler->addHtmlContent($html, 'UTF-8');

        $stories = $crawler->filter(".story")->each(function (Crawler $node, $i) {
            $story['title'] = $node->filter(".story__title")->text();
            $story['url'] = 'https://yandex.ru'.$node->filter(".story__title > .link")->attr('href');
            return $story;
        });
        dd($stories);
    */
}
