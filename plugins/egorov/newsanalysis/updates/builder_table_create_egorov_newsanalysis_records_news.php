<?php namespace Egorov\NewsAnalysis\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateEgorovNewsanalysisRecordsNews extends Migration
{
    public function up()
    {
        Schema::create('egorov_newsanalysis_records_news', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('records_id');
            $table->integer('news_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('egorov_newsanalysis_records_news');
    }
}
