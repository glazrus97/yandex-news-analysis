<?php namespace Egorov\NewsAnalysis\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateEgorovNewsanalysisNewsData extends Migration
{
    public function up()
    {
        Schema::create('egorov_newsanalysis_news_data', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('news_id');
            $table->integer('likes');
            $table->integer('comments');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('egorov_newsanalysis_news_data');
    }
}
