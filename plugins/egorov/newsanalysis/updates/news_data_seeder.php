<?php namespace Egorov\NewsAnalysis\Updates;

use Seeder;
use Egorov\NewsAnalysis\Models\News;
use Egorov\NewsAnalysis\Models\NewsData;

class SeedNewsDataTable extends Seeder
{
    public function run()
    {
        $news = News::all();
        foreach ($news as $n) {
            $link = $n->url;
            $html = file_get_contents($link);

            NewsData::create([
                'news_id' => $n->id,
                'likes' => random_int(1, 10000),
                'comments' => random_int(1, 10000),
                'position' => random_int(1, 9),
            ]);
        }
    }
}