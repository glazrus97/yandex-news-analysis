<?php namespace Egorov\NewsAnalysis\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateEgorovNewsanalysisRubrics extends Migration
{
    public function up()
    {
        Schema::create('egorov_newsanalysis_rubrics', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('url');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('egorov_newsanalysis_rubrics');
    }
}
