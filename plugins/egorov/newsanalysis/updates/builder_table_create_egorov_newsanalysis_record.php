<?php namespace Egorov\NewsAnalysis\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateEgorovNewsanalysisRecord extends Migration
{
    public function up()
    {
        Schema::create('egorov_newsanalysis_record', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('request_id');
            $table->text('comments');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('egorov_newsanalysis_record');
    }
}
