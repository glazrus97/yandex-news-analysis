<?php namespace Egorov\NewsAnalysis\Updates;

use Seeder;
use Egorov\NewsAnalysis\Models\News;
use Egorov\NewsAnalysis\Models\Media;
use Jenssegers\Date\Date;
use Symfony\Component\DomCrawler\Crawler;

class SeedAutoNewsTable extends Seeder
{
    public function run()
    {
        $link = 'https://yandex.ru/news/rubric/auto';
        $html = file_get_contents($link);
        $crawler = new Crawler(null, $link);
        $crawler->addHtmlContent($html, 'UTF-8');

        $stories = $crawler->filter(".story")->each(function (Crawler $node, $i) {
            $story['url'] = 'https://yandex.ru' . $node->filter(".story__title > .link")->attr('href');
            $storyCrawler = new Crawler(null, $story['url']);
            $storyCrawler->addHtmlContent(file_get_contents($story['url']), 'UTF-8');

            News::create([
                'title' => $node->filter(".story__title")->text(),
                'url' => explode('?', $story['url'])[0],
                'content' => $storyCrawler->filter(".doc__text")->text(),
                'rubric_id' => 8,
                'media_id' => Media::firstOrCreate(['name' => $storyCrawler->filter('.story__head-image')->attr('alt')])->id,
            ]);
        });
    }
}