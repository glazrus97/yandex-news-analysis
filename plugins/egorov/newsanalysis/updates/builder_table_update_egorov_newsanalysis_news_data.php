<?php namespace Egorov\NewsAnalysis\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateEgorovNewsanalysisNewsData extends Migration
{
    public function up()
    {
        Schema::table('egorov_newsanalysis_news_data', function($table)
        {
            $table->integer('position');
        });
    }
    
    public function down()
    {
        Schema::table('egorov_newsanalysis_news_data', function($table)
        {
            $table->dropColumn('position');
        });
    }
}
