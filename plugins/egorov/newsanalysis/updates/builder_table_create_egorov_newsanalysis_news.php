<?php namespace Egorov\NewsAnalysis\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateEgorovNewsanalysisNews extends Migration
{
    public function up()
    {
        Schema::create('egorov_newsanalysis_news', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('title');
            $table->text('url');
            $table->text('content');
            $table->integer('rubric_id');
            $table->integer('media_id');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('egorov_newsanalysis_news');
    }
}
