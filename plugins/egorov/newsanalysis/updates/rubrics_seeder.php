<?php namespace Egorov\NewsAnalysis\Updates;

use Seeder;
use Egorov\NewsAnalysis\Models\Rubrics;

class SeedRubricsTable extends Seeder
{
    public function run()
    {
        Rubrics::create([
            'name' => 'Общество',
            'url' => 'https://yandex.ru/news/rubric/society'
        ]);
        Rubrics::create([
            'name' => 'Политика',
            'url' => 'https://yandex.ru/news/rubric/politics'
        ]);
        Rubrics::create([
            'name' => 'Экономика',
            'url' => 'https://yandex.ru/news/rubric/business'
        ]);
        Rubrics::create([
            'name' => 'Происшествия',
            'url' => 'https://yandex.ru/news/rubric/incident'
        ]);
        Rubrics::create([
            'name' => 'Культура',
            'url' => 'https://yandex.ru/news/rubric/culture'
        ]);
        Rubrics::create([
            'name' => 'Технологии',
            'url' => 'https://yandex.ru/news/rubric/computers'
        ]);
        Rubrics::create([
            'name' => 'Наука',
            'url' => 'https://yandex.ru/news/rubric/science'
        ]);
        Rubrics::create([
            'name' => 'Авто',
            'url' => 'https://yandex.ru/news/rubric/auto'
        ]);
    }
}