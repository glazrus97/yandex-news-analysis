<?php

namespace Egorov\NewsAnalysis\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Egorov\NewsAnalysis\Models\News as NewsModel;
use Egorov\NewsAnalysis\Models\Media;
use Symfony\Component\DomCrawler\Crawler;

class News extends Controller
{
    public $implement = ['Backend\Behaviors\ListController',        'Backend\Behaviors\FormController',        'Backend\Behaviors\ReorderController',    'Backend.Behaviors.RelationController',         'Backend.Behaviors.ImportExportController',];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';
    public $relationConfig = 'config_relation.yaml';
    public $importExportConfig = 'config_import_export.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Egorov.NewsAnalysis', 'news');
    }

    public function diagrams() {
        $countByRybric = NewsModel::with('rubric')->get()->groupBy('rubric_id');
        $countByMedia = NewsModel::with('media')->get()->groupBy('media_id');
        $this->vars['countByRybrics'] = $countByRybric;
        $this->vars['countByMedias'] = $countByMedia;
    }

    public function onUpdateData() {
        $link = 'https://yandex.ru/news/rubric/society';
        $html = file_get_contents($link);
        $crawler = new Crawler(null, $link);
        $crawler->addHtmlContent($html, 'UTF-8');

        $stories = $crawler->filter(".story")->each(function (Crawler $node, $i) {
            $story['url'] = 'https://yandex.ru' . $node->filter(".story__title > .link")->attr('href');
            $storyCrawler = new Crawler(null, $story['url']);
            $storyCrawler->addHtmlContent(file_get_contents($story['url']), 'UTF-8');

            NewsModel::create([
                'title' => $node->filter(".story__title")->text(),
                'url' => explode('?', $story['url'])[0],
                'content' => $storyCrawler->filter(".doc__text")->text(),
                'rubric_id' => 1,
                'media_id' => Media::firstOrCreate(['name' => $storyCrawler->filter('.story__head-image')->attr('alt')])->id ?? 1,
            ])->save();
        });

        $link = 'https://yandex.ru/news/rubric/politics';
        $html = file_get_contents($link);
        $crawler = new Crawler(null, $link);
        $crawler->addHtmlContent($html, 'UTF-8');

        $stories = $crawler->filter(".story")->each(function (Crawler $node, $i) {
            $story['url'] = 'https://yandex.ru' . $node->filter(".story__title > .link")->attr('href');
            $storyCrawler = new Crawler(null, $story['url']);
            $storyCrawler->addHtmlContent(file_get_contents($story['url']), 'UTF-8');

            News::create([
                'title' => $node->filter(".story__title")->text(),
                'url' => explode('?', $story['url'])[0],
                'content' => $storyCrawler->filter(".doc__text")->text(),
                'rubric_id' => 2,
                'media_id' => Media::firstOrCreate(['name' => $storyCrawler->filter('.story__head-image')->attr('alt')])->id ?? 1,
            ]);
        })->save();

        return $this->listRefresh();
    }
}
