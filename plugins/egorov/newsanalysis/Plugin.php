<?php namespace Egorov\NewsAnalysis;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Egorov\NewsAnalysis\Components\RequestsForm' => 'RequestsForm',
        ];
    }

    public function registerSettings()
    {
    }
}
