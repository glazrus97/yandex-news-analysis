<?php

namespace Egorov\NewsAnalysis\Components;

use Cms\Classes\ComponentBase;
use Egorov\NewsAnalysis\Models\Requests;

class RequestsForm extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'RequestsForm Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onClick()
    {
        $data = post();

        Requests::create($data)->save();
    }
}
